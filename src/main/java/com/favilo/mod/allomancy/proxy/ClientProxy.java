package com.favilo.mod.allomancy.proxy;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.favilo.mod.allomancy.entities.Metal;
import com.favilo.mod.allomancy.items.Potions;

import net.minecraft.client.Minecraft;
import net.minecraft.client.resources.model.ModelBakery;
import net.minecraft.client.resources.model.ModelResourceLocation;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.IThreadListener;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class ClientProxy extends CommonProxy {
  private static final Logger logger = LogManager.getFormatterLogger(ClientProxy.class);
  private static final Minecraft mc = Minecraft.getMinecraft();

  @Override
  public void registerModels() {
    ModelResourceLocation resourceLocation =
        new ModelResourceLocation("bottle_drinkable", "inventory");
    for (Metal m : Metal.values()) {
      Minecraft.getMinecraft().getRenderItem().getItemModelMesher().register(Potions.metalVial,
          m.ordinal(), resourceLocation);
    }
  }

  @Override
  public EntityPlayer getPlayerEntity(MessageContext ctx) {
    logger.info("Retrieving player from ClientProxy for message on side " + ctx.side);
    if (ctx.side.isClient()) {
      return mc.thePlayer;
    }
    return super.getPlayerEntity(ctx);
  }

  @Override
  public IThreadListener getThreadFromContext(MessageContext ctx) {
    return ctx.side.isClient() ? mc : super.getThreadFromContext(ctx);
  }
}
