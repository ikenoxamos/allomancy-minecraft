package com.favilo.mod.allomancy.network.packet.client;

import java.io.IOException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.favilo.mod.allomancy.entities.MetalbornPlayer;
import com.favilo.mod.allomancy.network.packet.AbstractMessage.AbstractClientMessage;

import io.netty.buffer.ByteBuf;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.network.PacketBuffer;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;
import net.minecraftforge.fml.relauncher.Side;

public class SyncPlayerPropsMessage extends AbstractClientMessage<SyncPlayerPropsMessage> {
  private static final Logger logger =
      LogManager.getFormatterLogger(SyncPlayerPropsMessage.class);

  private NBTTagCompound data;

  public SyncPlayerPropsMessage() {}

  public SyncPlayerPropsMessage(EntityPlayer player) {
    data = new NBTTagCompound();
    MetalbornPlayer.get(player).saveNBTData(data);
  }

  @Override
  protected void read(PacketBuffer buffer) throws IOException {
    data = buffer.readNBTTagCompoundFromBuffer();
  }

  @Override
  protected void write(PacketBuffer buffer) throws IOException {
    buffer.writeNBTTagCompoundToBuffer(data);
  }

  @Override
  public void process(EntityPlayer player, Side side) {
    logger.info("Synchronizing extended properties data on CLIENT");
    MetalbornPlayer.get(player).loadNBTData(data);
  }
}
