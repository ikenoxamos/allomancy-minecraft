package com.favilo.mod.allomancy.network;

import com.favilo.mod.allomancy.Constants;
import com.favilo.mod.allomancy.network.packet.AbstractMessage;
import com.favilo.mod.allomancy.network.packet.client.SyncAbilityPropsMessage;
import com.favilo.mod.allomancy.network.packet.client.SyncPlayerPropsMessage;

import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import net.minecraftforge.fml.relauncher.Side;

public class PacketDispatcher {

  // Can't go larger than 255
  private static int packetId = 0;

  /**
   * The SimpleNetworkWrapper instance that is used to register and send packets.
   */
  private static final SimpleNetworkWrapper dispatcher =
      NetworkRegistry.INSTANCE.newSimpleChannel(Constants.MODID);

  public static final void registerPackets() {
    PacketDispatcher.registerMessage(SyncPlayerPropsMessage.class);
    PacketDispatcher.registerMessage(SyncAbilityPropsMessage.class);
  }

  private static final <T extends AbstractMessage<T> & IMessageHandler<T, IMessage>> void registerMessage(
      Class<T> cls) {
    if (AbstractMessage.AbstractClientMessage.class.isAssignableFrom(cls)) {
      PacketDispatcher.dispatcher.registerMessage(cls, cls, packetId++, Side.CLIENT);
    } else if (AbstractMessage.AbstractServerMessage.class.isAssignableFrom(cls)) {
      PacketDispatcher.dispatcher.registerMessage(cls, cls, packetId++, Side.SERVER);
    } else {
      PacketDispatcher.dispatcher.registerMessage(cls, cls, packetId, Side.CLIENT);
      PacketDispatcher.dispatcher.registerMessage(cls, cls, packetId++, Side.SERVER);
    }
  }

  /**
   * Send this message to the specified player. See
   * {@link SimpleNetworkWrapper#sendTo(IMessage, EntityPlayerMP)}
   */
  public static final void sendTo(IMessage message, EntityPlayerMP player) {
    PacketDispatcher.dispatcher.sendTo(message, player);
  }

  /**
   * Send this message to everyone. See {@link SimpleNetworkWrapper#sendToAll(IMessage)}
   */
  public static void sendToAll(IMessage message) {
    PacketDispatcher.dispatcher.sendToAll(message);
  }

  /**
   * Send this message to everyone within a certain range of a point. See
   * {@link SimpleNetworkWrapper#sendToDimension(IMessage, NetworkRegistry.TargetPoint)}
   */
  public static final void sendToAllAround(IMessage message, NetworkRegistry.TargetPoint point) {
    PacketDispatcher.dispatcher.sendToAllAround(message, point);
  }

  /**
   * Sends a message to everyone within a certain range of the coordinates in the same dimension.
   */
  public static final void sendToAllAround(IMessage message, int dimension, double x, double y,
      double z,

  double range) {
    PacketDispatcher.sendToAllAround(message, new NetworkRegistry.TargetPoint(dimension, x, y, z,

    range));
  }

  /**
   * Sends a message to everyone within a certain range of the player provided.
   */
  public static final void sendToAllAround(IMessage message, EntityPlayer player, double range) {
    PacketDispatcher.sendToAllAround(message, player.worldObj.provider.getDimensionId(),
        player.posX, player.posY, player.posZ, range);
  }

  /**
   * Send this message to everyone within the supplied dimension. See
   * {@link SimpleNetworkWrapper#sendToDimension(IMessage, int)}
   */
  public static final void sendToDimension(IMessage message, int dimensionId) {
    PacketDispatcher.dispatcher.sendToDimension(message, dimensionId);
  }

  /**
   * Send this message to the server. See {@link SimpleNetworkWrapper#sendToServer(IMessage)}
   */
  public static final void sendToServer(IMessage message) {
    PacketDispatcher.dispatcher.sendToServer(message);
  }
}
