package com.favilo.mod.allomancy.entities;

import java.awt.Color;

import net.minecraft.item.ItemStack;

public enum Metal {
  // Internal Physical
  PEWTER(new Color(157, 154, 150)), TIN(new Color(211, 212, 213)),

  // External Physical
  STEEL(new Color(224, 223, 219)), IRON(new Color(67, 75, 77)),

  // Internal Mental
  COPPER(new Color(184, 115, 51)), BRONZE(new Color(205, 127, 50)),

  // External Mental
  BRASS(new Color(181, 166, 66)), ZINC(new Color(186, 196, 200));

  private final Color color;

  Metal(Color color) {
    this.color = color;
  }

  public String toString() {
    return name().toLowerCase();
  }

  public static Metal fromMetadata(int meta) {
    return values()[meta];
  }

  public static Metal fromStack(ItemStack stack) {
    int metadata = stack.getMetadata() < 0 ? 0 : stack.getMetadata();
    return fromMetadata(metadata);
  }

  public int getColorRGB() {
    return color.getRGB();
  }
}