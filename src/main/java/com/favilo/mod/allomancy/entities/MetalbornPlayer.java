package com.favilo.mod.allomancy.entities;

import java.util.Map;

import com.favilo.mod.allomancy.network.PacketDispatcher;
import com.favilo.mod.allomancy.network.packet.client.SyncPlayerPropsMessage;
import com.favilo.mod.allomancy.proxy.CommonProxy;
import com.google.common.collect.ImmutableMap;
import com.google.common.collect.ImmutableMap.Builder;

import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraft.world.World;
import net.minecraftforge.common.IExtendedEntityProperties;
import net.minecraftforge.fml.common.FMLCommonHandler;

public class MetalbornPlayer implements IExtendedEntityProperties {

  private final static String EXT_PROP_NAME = "MetalbornEntity";

  private final EntityPlayer player;

  private Map<Metal, Ability> abilities;

  private MetalbornPlayer(EntityPlayer player) {
    this.player = player;
    Builder<Metal, Ability> builder = ImmutableMap.<Metal, Ability>builder();
    for (Metal metal : Metal.values()) {
      builder.put(metal, new Ability(player, metal));
    }
    abilities = builder.build();
  }

  public static MetalbornPlayer register(EntityPlayer player) {
    player.registerExtendedProperties(EXT_PROP_NAME, new MetalbornPlayer(player));
    return MetalbornPlayer.get(player);
    // Uncomment when we have the ability to get Misting powers
    // new MetalbornEntityProperties(player));
  }

  public static MetalbornPlayer get(Entity player) {
    if (player instanceof EntityPlayer) {
      return (MetalbornPlayer) player.getExtendedProperties(EXT_PROP_NAME);
    }
    return null;
  }

  public MetalbornPlayer makeMistborn() {
    for (Ability ability : abilities.values()) {
      ability.setStrength(10.0);
    }
    return this;
  }

  public Ability getMetal(Metal metal) {
    return abilities.get(metal);
  }

  @Override
  public void saveNBTData(NBTTagCompound compound) {
    NBTTagCompound properties = new NBTTagCompound();

    for (Ability ability : abilities.values()) {
      NBTTagCompound abilityProps = new NBTTagCompound();
      ability.saveNBTData(abilityProps);
      properties.setTag(ability.getMetal().name(), abilityProps);
    }

    compound.setTag(EXT_PROP_NAME, properties);
  }

  @Override
  public void loadNBTData(NBTTagCompound compound) {
    NBTTagCompound properties = (NBTTagCompound) compound.getTag(EXT_PROP_NAME);

    for (Metal m : Metal.values()) {
      NBTTagCompound metalProperties = (NBTTagCompound) properties.getTag(m.name());
      Ability ability = abilities.get(m);
      ability.loadNBTData(metalProperties);
    }
  }
  
  public void copy(MetalbornPlayer props) {
    
  }

  private static final String getSaveKey(EntityPlayer player) {
    return player.getName() + ":" + EXT_PROP_NAME;
  }

  @Override
  public void init(Entity entity, World world) {
    // Empty on purpose
  }

  public void addMetal(Metal m) {
    Ability ability = abilities.get(m);
    ability.modifyCurrent(100);
    ability.syncToClient();
  }

  public void syncToClient() {
    if (FMLCommonHandler.instance().getEffectiveSide().isServer()) {
      PacketDispatcher.sendTo(new SyncPlayerPropsMessage(player), (EntityPlayerMP) player);
    }
  }
}
